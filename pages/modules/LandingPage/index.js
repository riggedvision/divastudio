import styles from "./index.module.css";

import Frame from "./components/Frame";

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';

function uuidv4() {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
    var r = (Math.random() * 16) | 0,
      v = c == "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}

export default function LandingPage() {
  const props = [
    {
      time: "20:14",
      title: "Dirty Boy humping bed while StepMom watching tv series",
      view: 17,
      thumbnail: "http://www.ecovillaggi.eu/images/demo/img/demo-img-1.jpg",
      videoPreview: "https://www.w3schools.com/html/movie.mp4",
      likes: 5,
      videoPreview: "",
      addToCart: true,
    },

    {
      time: "20:14",
      title: "Dirty Boy humping bed while StepMom watching tv series",
      view: 21,
      thumbnail: "http://www.ecovillaggi.eu/images/demo/img/demo-img-1.jpg",
      videoPreview: "https://www.w3schools.com/html/movie.mp4",
      likes: 2,
      videoPreview: "",
      addToCart: true,
    },

    {
      time: "20:14",
      title: "Dirty Boy humping bed while StepMom watching tv series",
      view: 25,
      thumbnail: "http://www.ecovillaggi.eu/images/demo/img/demo-img-1.jpg",
      videoPreview: "https://www.w3schools.com/html/movie.mp4",
      likes: 18,
      videoPreview: "",
      addToCart: true,
    },

    {
      time: "20:14",
      title: "Dirty Boy humping bed while StepMom watching tv series",
      view: 45,
      thumbnail: "http://www.ecovillaggi.eu/images/demo/img/demo-img-1.jpg",
      videoPreview: "https://www.w3schools.com/html/movie.mp4",
      likes: 25,
      videoPreview: "",
      addToCart: true,
    },

    {
      time: "20:14",
      title: "Dirty Boy humping bed while StepMom watching tv series",
      view: 29,
      thumbnail: "http://www.ecovillaggi.eu/images/demo/img/demo-img-1.jpg",
      videoPreview: "https://www.w3schools.com/html/movie.mp4",
      likes: 12,
      videoPreview: "",
      addToCart: true,
    },

    {
      time: "20:14",
      title: "Dirty Boy humping bed while StepMom watching tv series",
      view: 144,
      thumbnail: "http://www.ecovillaggi.eu/images/demo/img/demo-img-1.jpg",
      videoPreview: "https://www.w3schools.com/html/movie.mp4",
      likes: 41,
      videoPreview: "",
      addToCart: true,
    },    {
      time: "20:14",
      title: "Dirty Boy humping bed while StepMom watching tv series",
      view: 17,
      thumbnail: "http://www.ecovillaggi.eu/images/demo/img/demo-img-1.jpg",
      videoPreview: "https://www.w3schools.com/html/movie.mp4",
      likes: 5,
      videoPreview: "",
      addToCart: true,
    },

    {
      time: "20:14",
      title: "Dirty Boy humping bed while StepMom watching tv series",
      view: 21,
      thumbnail: "http://www.ecovillaggi.eu/images/demo/img/demo-img-1.jpg",
      videoPreview: "https://www.w3schools.com/html/movie.mp4",
      likes: 2,
      videoPreview: "",
      addToCart: true,
    },

    {
      time: "20:14",
      title: "Dirty Boy humping bed while StepMom watching tv series",
      view: 25,
      thumbnail: "http://www.ecovillaggi.eu/images/demo/img/demo-img-1.jpg",
      videoPreview: "https://www.w3schools.com/html/movie.mp4",
      likes: 18,
      videoPreview: "",
      addToCart: true,
    },

    {
      time: "20:14",
      title: "Dirty Boy humping bed while StepMom watching tv series",
      view: 45,
      thumbnail: "http://www.ecovillaggi.eu/images/demo/img/demo-img-1.jpg",
      videoPreview: "https://www.w3schools.com/html/movie.mp4",
      likes: 25,
      videoPreview: "",
      addToCart: true,
    },

    {
      time: "20:14",
      title: "Dirty Boy humping bed while StepMom watching tv series",
      view: 29,
      thumbnail: "http://www.ecovillaggi.eu/images/demo/img/demo-img-1.jpg",
      videoPreview: "https://www.w3schools.com/html/movie.mp4",
      likes: 12,
      videoPreview: "",
      addToCart: true,
    },

    {
      time: "20:14",
      title: "Dirty Boy humping bed while StepMom watching tv series",
      view: 144,
      thumbnail: "http://www.ecovillaggi.eu/images/demo/img/demo-img-1.jpg",
      videoPreview: "https://www.w3schools.com/html/movie.mp4",
      likes: 41,
      videoPreview: "",
      addToCart: true,
    }
  ];

  const frames = props.map((prop) => {
    return <Frame {...prop} key={uuidv4()} />;
  });

  return (
    <Container>
      <div className={styles["frame-container"]}>
        <Grid container spacing={4}>
            {frames}
        </Grid>
      </div>
    </Container>
  )
}
