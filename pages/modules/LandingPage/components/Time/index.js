import styles from "./index.module.css";

export default function Time({ time }) {
  return (
    <div className={styles["frame-time"]}>
      {time}
    </div>
  );
}
