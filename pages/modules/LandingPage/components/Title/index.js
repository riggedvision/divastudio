import styles from "./index.module.css";

export default function Title({ title }) {
  return (
    <div className={styles["frame-title"]}>
      {title}
    </div>
  );
}
