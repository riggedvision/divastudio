import styles from "./index.module.css";

import Likes from "../Likes";
import Time from "../Time";
import Title from "../Title";
import Video from "../Video";
import Views from "../Views";

import Grid from '@material-ui/core/Grid';

export default function Frame({
  time,
  title,
  view,
  thumbnail,
  likes,
  videoPreview,
  addToCart,
}) {
  return (
    <Grid item lg={2} md={4} xs={12} className={styles["frame-view"]}>
      <div className={styles["frame-video-container"]}>
        <Video thumbnail={thumbnail} videoPreview={videoPreview} />
        <Time time={time} />
      </div>
      <Title title={title} />
      <Grid container justify="space-between">
        <Grid item>
          <Likes likes={likes} />
        </Grid>
        <Grid item>
          <Views view={view} />
        </Grid>
      </Grid>
    </Grid>
  );
}
