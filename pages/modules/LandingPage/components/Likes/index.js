import styles from "./index.module.css";

export default function Likes({ likes }) {
  // const likes = props.likes;
  
  return (
    <div className={styles["frame-likes"]}>
      {likes} likes
    </div>
  );
}
