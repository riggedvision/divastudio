import styles from "./index.module.css";

export default function Views({ view }) {
  return (
    <div className={styles["frame-views"]}>
      {view} views
    </div>
  );
}
