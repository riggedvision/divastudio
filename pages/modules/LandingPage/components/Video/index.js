import styles from "./index.module.css";
import React, { useRef } from 'react';

export default function Video({ thumbnail, videoPreview }) {
  const playVideo = useRef(null);

  return (
    <div className={styles["frame-video"]}>
      <video width="320" height="240" autoPlay muted className={styles["frame-video-player"]} ref={playVideo}>
        <source src="https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_640_3MG.mp4" type="video/mp4" />
        <source src="https://file-examples-com.github.io/uploads/2018/04/file_example_OGG_640_2_7mg.ogg" type="video/ogg" />
        Your browser does not support the video tag.
      </video>
      <img className={styles["frame-video-img"]} src={thumbnail} alt="demo img"
        onMouseEnter={() => {
          console.log(playVideo);
        }}
      />
    </div>
  );
}
