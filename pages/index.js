import styles from "../styles/Home.module.css";

import LandingPage from "./modules/LandingPage/index";

export default function Home() {
  return (
    <div className={styles.container}>
      <LandingPage />
    </div>
  );
}
